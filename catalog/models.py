from django.db import models


class Product(models.Model):
    name = models.CharField('Название продукта', max_length=300)
    glycemic_index = models.PositiveSmallIntegerField('Гликемический индекс', null=True, blank=True)
    carbohydrate = models.DecimalField('Углеводы на 100 грамм', max_digits=10, decimal_places=1, default=0.0)
    xe_count = models.DecimalField('Число xe на 100 грамм', max_digits=10, decimal_places=1, default=0.0)

    def calc_xe(self, portion, value):
        if portion == 'gr':
            if self.xe_count:
                xe_one_gr = self.xe_count / 100
                result = xe_one_gr * value
            else:
                result = 0
        else:
            portion = self.portions.get(portion_type=portion)
            result = portion.xe_count * value
        return result

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Продукт'
        verbose_name_plural = 'Продукты'
        ordering = ('name', )


class ProductPortion(models.Model):

    class TypePortion:
        GLASS = 0
        TABLESPOON = 1
        TEASPOON = 2
        STANDART = 3
        STUFF = 4

    PORTION_TYPE_CHOICE = (
        (TypePortion.GLASS, 'Стакан'),
        (TypePortion.TABLESPOON, 'Столовая ложка'),
        (TypePortion.TEASPOON, 'Чайная ложка'),
        (TypePortion.STANDART, 'Стандартная порция'),
        (TypePortion.STUFF, 'Штучное измерение')
    )

    product = models.ForeignKey(Product, verbose_name='Продукт', related_name='portions', on_delete=models.CASCADE)
    portion_type = models.SmallIntegerField('Тип порции', choices=PORTION_TYPE_CHOICE, default=TypePortion.GLASS)
    weight = models.DecimalField('Вес', max_digits=10, decimal_places=2, default='0.0')
    xe_count = models.DecimalField('Число хлебных единиц', max_digits=10, decimal_places=1, default='0.0')

    def __str__(self):
        return self.get_portion_type_display()

    class Meta:
        verbose_name = 'Порция'
        verbose_name_plural = 'Порции'
