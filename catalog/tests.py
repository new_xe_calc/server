from decimal import Decimal

from django.test import TestCase
from rest_framework import status
from rest_framework.test import APIClient

from catalog.models import Product, ProductPortion


class ProductTestMixin:

    def create_product(self, additional_data=None):
        data = {'name': 'Продукт', 'xe_count': 5}
        if additional_data:
            data.update(additional_data)
        return Product.objects.create(**data)

    def create_product_with_portions(self, portions_list, data_for_product=None):
        product = self.create_product(data_for_product)
        if portions_list:
            for portion in portions_list:
                portion.update({'product': product})
                ProductPortion.objects.create(**portion)
        return product


class ModelProductTestCase(TestCase):

    def test_calc_gram_product(self):
        product = Product.objects.create(name='Мандарин', xe_count=5.5)
        target_xe_count = 8.25

        result = product.calc_xe('gr', 150)

        self.assertEqual(result, target_xe_count)

    def test_calc_glass_calc(self):
        product = Product.objects.create(name='Сок')
        ProductPortion.objects.create(product=product, xe_count=2.9, portion_type=ProductPortion.TypePortion.GLASS)
        target_xe_count = Decimal('5.8')

        result = product.calc_xe(ProductPortion.TypePortion.GLASS, 2)
        self.assertEqual(result, target_xe_count)


class ProductGetApiTest(TestCase, ProductTestMixin):

    def setUp(self):
        self.client = APIClient()

    def test_get_products_list(self):
        self.create_product()
        self.create_product()
        result = self.client.get('/api/products/')

        self.assertEqual(result.status_code, status.HTTP_200_OK)
        self.assertEqual(len(result.data), 2)

    def test_search_product(self):
        self.create_product(additional_data={'name': 'Хлеб'})
        self.create_product(additional_data={'name': 'Молоко'})
        self.create_product(additional_data={'name': 'Мясо'})

        result = self.client.get('/api/products/?search=м')
        self.assertEqual(result.status_code, status.HTTP_200_OK)
        self.assertEqual(len(result.data), 2)

        result = self.client.get('/api/products/?search=хле')
        self.assertEqual(result.status_code, status.HTTP_200_OK)
        self.assertEqual(len(result.data), 1)


class ProductPortionGetApiTest(TestCase, ProductTestMixin):

    def setUp(self):
        self.client = APIClient()

    def test_get_portions_list(self):
        product = self.create_product_with_portions(portions_list=[
            {'portion_type': ProductPortion.TypePortion.STANDART, 'weight': 10.5, 'xe_count': 8.5},
            {'portion_type': ProductPortion.TypePortion.GLASS, 'weight': 55, 'xe_count': 15.3}
        ])
        result = self.client.get('/api/portions/?product={}'.format(product.pk))

        self.assertEqual(result.status_code, status.HTTP_200_OK)
        self.assertEqual(len(result.data), 2)
