from rest_framework.pagination import LimitOffsetPagination


class ProductPaginator(LimitOffsetPagination):
    page_size = 30
