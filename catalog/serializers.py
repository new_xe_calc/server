from rest_framework import serializers

from catalog.models import Product, ProductPortion


class ProductSerializer(serializers.ModelSerializer):

    class Meta:
        model = Product
        fields = '__all__'


class ProductPortionSerializer(serializers.ModelSerializer):
    portion_name = serializers.SerializerMethodField()

    def get_portion_name(self, obj):
        return obj.get_portion_type_display()

    class Meta:
        model = ProductPortion
        fields = ['portion_type', 'weight', 'xe_count', 'portion_name']
