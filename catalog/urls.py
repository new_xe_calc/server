from django.urls import path, include
from rest_framework import routers

from .views import ProductViewSet, ProductPortionListViewSet

router = routers.DefaultRouter()
router.register(r'products', ProductViewSet)
router.register(r'portions', ProductPortionListViewSet)

urlpatterns = [
    path('', include(router.urls))
]
