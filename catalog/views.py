from rest_framework import mixins, filters
from rest_framework.response import Response
from rest_framework.viewsets import GenericViewSet

from catalog.models import Product, ProductPortion
from catalog.paginators import ProductPaginator
from catalog.serializers import ProductSerializer, ProductPortionSerializer


class ProductViewSet(mixins.RetrieveModelMixin, mixins.ListModelMixin, GenericViewSet):
    serializer_class = ProductSerializer
    queryset = Product.objects.all()
    pagination_class = ProductPaginator
    filter_backends = (filters.SearchFilter,)
    search_fields = ('name',)

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())
        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)


class ProductPortionListViewSet(mixins.ListModelMixin, GenericViewSet):
    serializer_class = ProductPortionSerializer
    queryset = ProductPortion.objects.all()

    def get_queryset(self):
        qs = self.queryset
        if self.request.GET.get('product') and self.request.GET['product'].isdigit():
            product_id = self.request.GET['product']
            qs = qs.filter(product=product_id)
        else:
            qs = ProductPortion.objects.none()
        return qs
