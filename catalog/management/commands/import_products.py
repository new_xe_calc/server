from django.core.management import BaseCommand

from decimal import *
from catalog.models import Product, ProductPortion
import json


class Command(BaseCommand):
    help = "Create products from file"

    def is_digit(self, val):
        try:
            float(val)
            return True
        except ValueError:
            return False

    def handle(self, *args, **options):
        with open('products.json') as f:
            data = json.loads(f.read())

        portions = {'glass': ProductPortion.TypePortion.GLASS, 'tablespoon': ProductPortion.TypePortion.TABLESPOON,
                    'teaspoon': ProductPortion.TypePortion.TEASPOON, 'standart': ProductPortion.TypePortion.STANDART,
                    'stuff': ProductPortion.TypePortion.STUFF}

        for item in data:
            product = Product()
            product.name = item['name']
            product.carbohydrate = Decimal(item['carbohydrates'])
            if item['glycemic_index']:
                product.glycemic_index = Decimal(item['glycemic_index'])
            product.xe_count = Decimal(item['xe_count'])
            product.save()

            for portion_type in portions.keys():
                weight_key = '{}_weight'.format(portion_type)
                xe_key = '{}_xe'.format(portion_type)
                if self.is_digit(item[weight_key]) or self.is_digit(item[xe_key].isdigit()):
                    product_portion = ProductPortion()
                    product_portion.product = product
                    product_portion.portion_type = portions[portion_type]

                    if self.is_digit(item[weight_key]):
                        product_portion.weight = Decimal(item[weight_key])
                    if self.is_digit(item[xe_key]):
                        product_portion.xe_count = Decimal(item[xe_key])
                    product_portion.save()
