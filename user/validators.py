from django.core.exceptions import ValidationError


def validate_not_empty(value):
    if not value:
        raise ValidationError('%(value)s is empty!', params={'value': value})
